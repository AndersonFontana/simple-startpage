const UNSPLASH_BASE = 'https://source.unsplash.com/768x432/?'

// const LANG = 'pt-BR'
var LANG = 'en-US'
var USERNAME = 'Anderson'
var ROTATE_IMG_HOURS = 4
var ARGS, IMG_URL

var div_bg
var div_overlay
var div_clock
var div_date
var div_greeting
var btn_info
var btn_refresh
var img_bg

var t // translations


Date.prototype.greeting = function() {
	var time = this.getHours()
	if (0 <= time && time <= 4) return t.greeting_late
	if (time < 8) return t.greeting_early
	if (time < 12) return t.greeting_morning
	if (time < 18) return t.greeting_afternoon
	if (time < 21) return t.greeting_evening
	return t.greeting_late
}

function startTime() {
	var today = new Date()

	const clock_options = { 'hour': '2-digit', 'minute': '2-digit', 'hour12': false}
	div_clock.innerHTML = today.toLocaleTimeString(t.display_clock, clock_options)

	const date_options = { 'weekday': 'long', 'year': 'numeric', 'month': 'long', 'day': 'numeric' }
	div_date.innerHTML = today.toLocaleDateString(t.display_date, date_options)

	div_greeting.innerHTML = `${t.hey_text}${ USERNAME ? ' '+USERNAME : ''}, ${today.greeting()}`

	setTimeout(startTime, 5000)
}

function setProperties() {
	// div_overlay.style.height = (window.innerHeight-50) +'px'
	btn_refresh.onclick = refreshImage

	btn_info.onclick = () => {
		var source = localStorage.getItem('source')
		if (source) {
			source = source.split('?')[0] + '?w=1920'
			var win = window.open(source, '_blank')
			win.focus()
		}
	}
}


function getImageB64 () {
	return new Promise((resolve, reject) => {
		fetch(IMG_URL).then(resp => {
			if (resp.status >= 400)
				resp.json().then(json => reject({'status': resp.status, 'response': json}))
			else {
				localStorage.setItem('source', resp.url)
				resp.blob().then(blob => {
					const reader = new window.FileReader()
					reader.readAsDataURL(blob)
					reader.onloadend = () => resolve(reader.result)
				})
			}
		})
	})
}

function request (url) {
	// var headers = new Headers({
	// 	"Content-Type": "text/html",
	// })

	// var fetch_conf = {
	// 	method: 'GET',
	// 	headers: headers,
	// 	mode: 'no-cors',
	// 	cache: 'default'
	// }

	return new Promise((resolve, reject) => {
		fetch('https://cors-anywhere.herokuapp.com/'+url).then(resp => {
			if (resp.status >= 400)
				resp.json().then(json => reject({'status': resp.status, 'response': json}))
			else
				resp.text().then(text => resolve(text))
		})

	})
}

function refreshImage() {
	btn_refresh.disabled = true
	btn_info.disabled = true
	btn_refresh.querySelector('span').innerText = '...'

	getImageB64().then(new_b64 => {
		localStorage.setItem('image', new_b64)
		localStorage.setItem('last_sync', new Date().toISOString())
		defineImage(new_b64)
	})
}


function getImage() {
	const local_b64 = localStorage.getItem('image')
	const last_sync = localStorage.getItem('last_sync')

	var hours = 0
	if (last_sync)
		hours = Math.abs(new Date() - new Date(last_sync)) / 36e5

	if (local_b64)
		defineImage(local_b64)

	if (!local_b64 || hours > ROTATE_IMG_HOURS)
		refreshImage()
}

function defineImage(source) {
	div_bg.className = ''
	img_bg.src = source
	// img_bg.src = './wallpaper.png'

	img_bg.onload = () => {
		div_bg.className = 'loaded'
		btn_refresh.disabled = false
		btn_info.disabled = false
		btn_refresh.querySelector('span').innerText = t.refresh_text
	}
}


function loadCotation() {
	request('http://www.cotapel.com.br/cotacao-grafico.php?limite=7&pCot_id=2').then(
		text => {
			var test = [...text.matchAll(/\s+\['(\d+.\d+.\d+)',\s+(\d+.\d+?),\s'<div style="/g,)]
			var data = []
			test.forEach(match => {
				data.push({
					'date': match[1],
					'price': match[2]
				})
			})
			console.log(data)


			var ctx = document.getElementById('cotation').getContext('2d');
			var chart = new Chart(ctx, {
				// The type of chart we want to create
				type: 'line',

				// The data for our dataset
				data: {
					labels: data.map(item => item.date),
					datasets: [{
						label: 'My First dataset',
						// backgroundColor: 'rgb(255, 99, 132)',
						borderColor: 'rgb(255, 99, 132)',
						data: data.map(item => item.price)
					}]
				},

				// Configuration options go here
				options: {}
			});

		}
	)

}


window.onload = () => {

	div_bg       = document.getElementById('bg')
	div_overlay  = document.getElementById('overlay')
	div_clock    = document.getElementById('clock')
	div_date     = document.getElementById('date')
	div_greeting = document.getElementById('greeting')
	btn_refresh  = document.getElementById('refresh')
	btn_info     = document.getElementById('info')
	img_bg       = document.getElementsByTagName('img')[0]


	ARGS = location.search.slice(1).split('&').map(p => p.split('=')).reduce((obj, [key, value]) => ({ ...obj, [key]: value }), {})
	if (ARGS.user)
		USERNAME = ARGS.user

	if (ARGS.lang)
		LANG = ARGS.lang

	if (ARGS.new_image)
		ROTATE_IMG_HOURS = ARGS.new_image

	if (ARGS.unsplash)
		IMG_URL = UNSPLASH_BASE + ARGS.unsplash
	else
		IMG_URL = UNSPLASH_BASE + 'nature,landscape'

	// loadCotation()

	fetch('./lang/'+ LANG +'.json', {mode: 'no-cors'})
	.then(resp => resp.json())
	.then(json => {
		// Load translation
		t = json

		// Load elements
		startTime()
		setProperties()
		getImage()
	})
}
